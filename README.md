# JSON with Ruby

### Create a JSON file 
  
* For Example file name _database.json_ and add :

 ```json
 {
  "Car" : "BMW",
  "Cities" : ["İstanbul", "Edirne", "İzmir"],
  "Books" : ["Nutuk", "Fahrenheit 451"]
 }
 ```


### Reading JSON file with ruby

* Install JSON and pp gem
pp gem is 'pretty print' gem for ruby objects

```bash
gem install json
gem install pp
```

### Requirements 

```ruby
require 'json'
require 'pp'
```

* Create a ruby file and add: 

```ruby
#!/usr/bin/env ruby

require 'json'
require 'pp'

json = File.read('database.json')
obj = JSON.parse(json)

pp obj # Using pp
``` 


### Output

![output.png](./output.png)

### Added new data into JSON file (_deneme.json_)

* Reading data existing JSON file'data
* Append entered data into array (_enteredData_)
* Append entered data(_enteredData_) into "readedData" 
* Write readedData to the JSON file.




  


